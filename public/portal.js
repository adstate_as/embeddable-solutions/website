(function () {
    let baseURL = 'https://minnesider.no'
    const env = getEnv();
    let cacheBustString = new Date().toISOString()

    if (env === 'dev') {
        baseURL = 'https://national-portal-fe.development.ads1.itpartner.no';
        cacheBustString = cacheBustString.slice(0, -8) // YYYY-MM-DDThh:mm
    } else if (env === 'test') {
        baseURL = 'https://national-portal-fe.test.ads1.itpartner.no';
        cacheBustString = cacheBustString.slice(0, -8) // YYYY-MM-DDThh:mm
    } else {
        cacheBustString = cacheBustString.slice(0, -14) // YYYY-MM-DD
    }

    const scriptElement = document.createElement("script");
    scriptElement.src = baseURL + "/embed/portal.js?v=" + cacheBustString;
    document.body.appendChild(scriptElement)

    function getEnv() {
        let env = null;

        const devEnvs = [
            '://localhost',
            '://dev.',
            '://review',
            'deploy-preview-',
            'file://',
        ];

        const testEnvs = [
            '://test.',
            '://sandbox.',
        ];

        if (env === null) {
            devEnvs.forEach(devEnv => {
                if (window.location.href.includes(devEnv)) {
                    env = 'dev'
                }
            })
        }

        if (env === null) {
            testEnvs.forEach(testEnv => {
                if (window.location.href.includes(testEnv)) {
                    env = 'test'
                }
            })
        }

        if (env === null) {
            env = 'prod'
        }

        return env
    }
})()
